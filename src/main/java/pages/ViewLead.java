package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethod;

public class ViewLead extends ProjectMethod{
	
	public ViewLead()
	{
		PageFactory.initElements(driver,this);
	}
	@FindBy(how=How.ID,using="viewLead_firstName_sp") WebElement elegetText;
	
	public LoginPage gettheText()
	{
		verifyExactText(elegetText,CreateLead.FirstName);
		return new LoginPage();
	}
}
