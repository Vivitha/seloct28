package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethod;

public class SwitchSecondWindow extends ProjectMethod{
	
	public SwitchSecondWindow()
	{
		PageFactory.initElements(driver,this);
	}
	
	@FindBy(how=How.XPATH,using="(//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first '])[1]/div/a") WebElement elePickLead;
	public FindLead switchWindow()
	{
		switchToWindow(1);
		return new FindLead();
	}
	
	public MergeLeads pickLead() {
		clickWitOutSnap(elePickLead);
		switchToWindow(0);
		return new MergeLeads();
	}


}