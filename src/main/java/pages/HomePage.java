package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethod;

public class HomePage extends ProjectMethod {

public HomePage()
{
	PageFactory.initElements(driver,this);
}

//Page Factory @FindBy -- > Similar to your locate Element
@FindBy(how=How.CLASS_NAME,using="decorativeSubmit") WebElement eleLogout;
@FindBy(how=How.LINK_TEXT,using="CRM/SFA") WebElement eleclicks;
//HomePage Click
public MyHome enterClick()
{
	click(eleclicks);
	return new MyHome();
}
//LogOut
public LoginPage Logout()

{
	click(eleLogout);
	return new LoginPage();
}
}
