package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethod;

public class MergeLeads extends ProjectMethod{

	public MergeLeads()
	{
		PageFactory.initElements(driver,this);
	}
	@FindBy(how=How.XPATH,using="//input[@id='partyIdFrom']/following-sibling::a/img") WebElement eleFromLead;
	@FindBy(how=How.XPATH,using="//input[@id='partyIdTo']/following-sibling::a/img") WebElement eleToLead;
	@FindBy(how=How.XPATH,using="(//a[text()='Merge'])[1]") WebElement eleMergeElement;
	public SwitchSecondWindow SearchFromLead()
	{
		click(eleFromLead);
		return new SwitchSecondWindow();
	}
	public SwitchSecondWindow SearchToLead()
	{
		click(eleToLead);
		return new SwitchSecondWindow();
	}
	
	public ViewLead clickMergeLead()
	{
		clickWitOutSnap(eleMergeElement);
		acceptAlert();
		return new ViewLead();
	}
	
}
