package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethod;

public class FindLead extends ProjectMethod {

	public FindLead()
	{
		PageFactory.initElements(driver,this);
	}
	@FindBy(how=How.XPATH,using="//input[@name='firstName']") WebElement eleFsearch;
	@FindBy(how=How.XPATH,using="//button[text()='Find Leads']") WebElement eleFindClick;
	
	public FindLead FNameSearch(String FirstName)
	{
		type(eleFsearch,FirstName);
		return this;
		
	}
	public SwitchSecondWindow clickFind() throws InterruptedException
	{
		click(eleFindClick);
		Thread.sleep(2000);
		return new SwitchSecondWindow();
	}

		
}
