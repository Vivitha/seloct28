package pages;

	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.support.FindBy;
	import org.openqa.selenium.support.How;
	import org.openqa.selenium.support.PageFactory;

	import wdMethods.ProjectMethod;

	public class LoginPage extends ProjectMethod {
		
		public LoginPage()
		{
			
			PageFactory.initElements(driver,this);
		}
		//Page Factory @FindBy -- > Similar to your locate Element
		@FindBy(how=How.ID,using="username") WebElement eleUserName;
		@FindBy(how=How.ID,using="password") WebElement elePassword;
		@FindBy(how=How.CLASS_NAME,using="decorativeSubmit") WebElement eleLogin;
		
		//Enter User Name
		public LoginPage enterUserName(String uName)

		{
			type(eleUserName,uName);
			return this;
		}
		//Enter Password
		public LoginPage enterPassword(String Password)
		{
			type(elePassword,Password);
			return this;
		}
		public HomePage clickLogin()
		{
			click(eleLogin);
			return new HomePage();
		}
	}


