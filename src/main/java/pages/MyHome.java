package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethod;

public class MyHome extends ProjectMethod{
	
	public MyHome()
	{
		PageFactory.initElements(driver,this);
	}
	@FindBy(how=How.LINK_TEXT,using="Leads") WebElement eleLeads;
	//Click Leads
	public MyLeads ClickLeads()
	{
		click(eleLeads);
		return new MyLeads();
	}

}
