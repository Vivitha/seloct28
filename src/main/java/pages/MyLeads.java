package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethod;

public class MyLeads extends ProjectMethod{
	
	public MyLeads()
	{
		PageFactory.initElements(driver,this);
	}
	@FindBy(how=How.LINK_TEXT,using="Create Lead") WebElement eleCLeads;
	@FindBy(how=How.LINK_TEXT,using="Merge Leads") WebElement eleMLeads;
	// Click CreateLead
	public CreateLead ClickCreateLead()
	{
		click(eleCLeads);
		return new CreateLead();
	}
	public MergeLeads clickMergeLead()
	{
		click(eleMLeads);
		return new MergeLeads();
	}

}
