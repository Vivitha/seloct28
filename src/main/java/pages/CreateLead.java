package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethod;

public class CreateLead extends ProjectMethod {
	public static String FirstName;
	public CreateLead()
	{
		PageFactory.initElements(driver,this);
	}
	//Page Factory @FindBy -- > Similar to your locate Element
			@FindBy(how=How.ID,using="createLeadForm_companyName") WebElement eleCompany;
			@FindBy(how=How.ID,using="createLeadForm_firstName") WebElement eleFname;
			@FindBy(how=How.ID,using="createLeadForm_lastName") WebElement eleLname;	
			@FindBy(how=How.CLASS_NAME,using="smallSubmit") WebElement eleCLogin;
			
			//Enter Company Name
			public CreateLead enterCompanyName(String CName)

			{
				type(eleCompany,CName);
				return this;
			}
			//Enter First Name
			public CreateLead enterFirstName(String  FName)
			{
				type(eleFname,FName);
				FirstName=FName;
				return this;
			}
			//Enter Last Name
			public CreateLead enterLastName(String LName)
			{
				type(eleLname,LName);
				return this;
			}
			public ViewLead clickLogin()
			{
				click(eleCLogin);
				return new ViewLead();
			}
		}
