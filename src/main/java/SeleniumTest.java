import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.flipkart.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("//button[@class='_2AkmmA _29YdH8']").click();
		driver.findElementByXPath("//input[@title='Search for products, brands and more']").sendKeys("iPhone X",
				Keys.ENTER);
		List<WebElement> priceElement = driver.findElementsByXPath("//div[@class='_1vC4OE']");
		List<String> price = new ArrayList<>();
		List<Integer> prc = new ArrayList<>();
		List<String> newPrice = new ArrayList<>();
		Set<Integer> uniquePrice=new TreeSet();
		
		int amount=0,len=0;
		for (WebElement eachElement : priceElement) {
			price.add(eachElement.getText());	
		}
		for (String rate : price) {
			newPrice.add(rate.replaceAll("[^0-9]", ""));
		}
		for(String r:newPrice)
			prc.add(Integer.parseInt(r));
		{
			
		}
		int firstMax=0;
		uniquePrice.addAll(prc);
		for (Integer eachPrice : uniquePrice) {
				if (eachPrice > firstMax) {
					firstMax = eachPrice;
				}
			}
			Integer secondMax = 0;

			for (Integer eachPrice : uniquePrice) {
				if (eachPrice > secondMax && eachPrice != firstMax) {
					secondMax = eachPrice;
				}

			}
			String secondMaxStr = secondMax.toString();

			System.out.println(secondMaxStr);
			List<WebElement> mobName = driver.findElementsByXPath("//div[@class='_1vC4OE']/preceding::a[1]");
			Map<WebElement, String> priceName = new LinkedHashMap<>();
			for (int i = 0; i < mobName.size(); i++) {
				priceName.put(mobName.get(i), newPrice.get(i));
			}
			for (Entry<WebElement, String> eachPrice : priceName.entrySet()) {
				if (eachPrice.getValue().contains(secondMaxStr)) {
					System.out.println(eachPrice.getKey().getText());
					eachPrice.getKey().click();
				}
			}

}
}
