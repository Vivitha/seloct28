package testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethod;

public class TC001_LoginLogout extends ProjectMethod{

	@BeforeTest
	public void setData()
	{
		testCaseName="Login Logout";
		testDesc ="Login into Leaftap";
		author="";
		category="";
		dataSheetName="TC001";
	}
	
	@Test(dataProvider="fetchData")
	public void loginLogout(String uName, String password)
	{
		new LoginPage()
		.enterUserName(uName)
		.enterPassword(password)
		.clickLogin()
		.Logout();
	
	}

}
