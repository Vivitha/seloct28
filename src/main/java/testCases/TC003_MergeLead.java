package testCases;

	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

import pages.FindLead;
import pages.LoginPage;
import pages.MergeLeads;
import wdMethods.ProjectMethod;

	public class TC003_MergeLead extends ProjectMethod{ 
		@BeforeTest
		public void setData()
		{
			testCaseName="Create Lead";
			testDesc ="Create Lead TestCases";
			author="Vivitha";
			category="Smoke";
			//dataSheetName="createLead";
		}
		
		@Test
		public void loginLogout() throws InterruptedException
		{
			new LoginPage()
			.enterUserName("DemoSalesManager")
			.enterPassword("crmsfa")
			.clickLogin()
			.enterClick()
			.ClickLeads()
			.clickMergeLead()
			.SearchFromLead()
			.switchWindow()
			.FNameSearch("Vivitha")
			.clickFind()
			.pickLead()
			.SearchToLead()
			.switchWindow()
			.FNameSearch("Vinoth")
			.clickFind()
			.pickLead();
			
		}
	}


