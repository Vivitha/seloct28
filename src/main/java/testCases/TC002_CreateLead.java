package testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethod;

public class TC002_CreateLead extends ProjectMethod{
	@BeforeTest
	public void setData()
	{
		testCaseName="Create Lead";
		testDesc ="Create Lead TestCases";
		author="Vivitha";
		category="Smoke";
		dataSheetName="createLead";
	}
	
	//@Test(dataProvider="fetchData")
	public void loginLogout(String CName,String FName, String LName)
	{
		new LoginPage()
		.enterUserName("DemoSalesManager")
		.enterPassword("crmsfa")
		.clickLogin()
		.enterClick()
		.ClickLeads()
		.ClickCreateLead()
		.enterCompanyName(CName)
		.enterFirstName(FName)
		.enterLastName(LName)
		.clickLogin()
		.gettheText();
	}
}
